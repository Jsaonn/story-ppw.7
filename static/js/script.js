var acc = document.getElementsByClassName("accordion")
var i;

$(document).ready(function() {
    for(i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");

            var panel = this.parentNode.nextElementSibling;
            if(panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }


    $('.down').click(function (e) {
        event.stopPropagation();
        var self = $(this),
            item = self.parents('div#acc'),
            swapWith = item.next();
        item.before(swapWith.detach());
    });
    $('.up').click(function (e) {
        event.stopPropagation();
        var self = $(this),
            item = self.parents('div#acc'),
            swapWith = item.prev();
        item.after(swapWith.detach());
    });
});