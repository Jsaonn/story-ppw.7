from django.urls import path
from . import views

app_name = "accordion"

urlpatterns = [
	path('acc/', views.index, name = 'index'),
]
