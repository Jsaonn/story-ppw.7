from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest

# Create your tests here.

class Test(TestCase):

    def test_url_ada(self):
        response = Client().get('/acc/')
        self.assertEqual(response.status_code, 200)

    def test_app_ada(self):
        found = resolve('/acc/')
        self.assertEqual(found.func, index)
    
    def test_template(self):
        response = Client().get('/acc/')
        self.assertTemplateUsed(response, 'index.html')
